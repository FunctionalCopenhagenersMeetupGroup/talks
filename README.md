Talks given at MF#K
===================

### 2017

#### 2017-10-31 "Joe" and Claes Worm: MirageOS and OCaml

* Slides ("Joe"): [local archive](2017-10-31/2017.10.mirage.mfk.pdf)

* Code (Claes Worm): [rand00 at GH](https://github.com/rand00/mirage_hands-on)

#### 2017-09-26 Mark Seemann: From dependency injection to dependency rejection

Mark share the same thoughts as [Martin Fowler][martinfowler] with regard of the
[problem of trying to make slides useful as documents][slideument]. As we
noticed, the slides use some animations and therefore don't work well as a PDF
file (static document), which is what MF#K request from the speakers after a
talk is given so everybody is able to see them.

As mentioned by Mark at the talk, he has an [infodeck][infodeck] on the subject
available on his blog:

* [From dependency injection to dependency rejection][blog]

[martinfowler]: https://twitter.com/martinfowler
[slideument]: https://martinfowler.com/bliki/Slideument.html
[infodeck]: https://martinfowler.com/bliki/Infodeck.html
[blog]: http://blog.ploeh.dk/2017/01/27/from-dependency-injection-to-dependency-rejection

#### 2017-06-27 Rasmus Lerchedahl Petersen: Untied Fixed Points (hands-on)

* Slides ([local archive](2017-06-27/RasmusLerchedahlPetersen_UntiedFixedPoints_Hands-On.pdf), [@yoff](https://yoff.github.io/untied/))

* Code:

  - F# script ([local archive](2017-06-27/code/untied.fsx), [@yoff](https://github.com/yoff/untied/blob/master/untied.fs))

  - Solutions ([local archive](2017-06-27/code/untied_solutions.fsx), [@yoff](https://github.com/yoff/untied/blob/master/untied_solutions.fs))

### 2016

### 2015

### 2014

### 2013

## Licensing

### Code

Unless otherwise mentioned, all code are public domain, which allows you to do
whatever you want with them.

### Media Content

In order to ensure that speakers are given credit for their work, the talks and
any other textual content on this site is licensed under the <a rel="license"
href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons
Attribution-ShareAlike 4.0 International License</a>, unless otherwise
mentioned.
